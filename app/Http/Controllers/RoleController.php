<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class RoleController extends Controller
{
    public function role(Request $request)
    {
          $user = $request->user();
          
            //    return view('home');
           //  return redirect('students');
        if($user && $user->role == 'gerant'){
            return view ('pharmacie.gerant');
         }   
        elseif($user && $user->role == 'superviseur')
        return view ('pharmacie.superviseur');
        else return redirect('/indexpro');     
    }

    public function getProduitList(Request $request)
        {
            $produits = DB::table("produits")
            ->where("prix",$request->prix)
            ->orderBy('name')
            ->pluck("name","id");
            return response()->json($produits);
        }


}