@extends('layouts.app')
  
@section('content')
   <!-- Content Wrapper. Contains page content -->
   
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="card card-info card-outline card-tabs">
            @include('inc.messages')
              <div class="card-body">
                  <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-info">
             <div class="card-header">
                <h3 class="card-title">Select Criteria</h3>
                 <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                  </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
              
              <form role="form" class="" method="post" action="">
              @csrf

              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong><label for="exampleInputEmail1">Name</label></strong>
                        <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter Name">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong><label for="exampleInputEmail1">prenom</label></strong>
                        <input type="text" name="quantite" class="form-control" id="exampleInputEmail1" placeholder="Enter Quantity">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong><label for="exampleInputEmail1">Prix scolarite</label></strong>
                        <input type="text" name="prix" class="form-control" id="exampleInputEmail1" placeholder="Enter the Price">
                    </div>
                </div>
        
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
              </form>
            <!-- /.card -->
          

        </div>
    </div>
          <!--/.col (left) -->
          <!-- right column -->
         
               
              <!-- /.card -->
            </div>
          </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
      <!-- Main content -->
      
  <!-- /.card-body -->
          </div>
        </div>
      </div>
    
      </div>
      <!-- /.card -->
      </div></div>

        
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
@endsection

    @section('special-scripts')

  <script type="text/javascript">
    $('#region').change(function(){
    var regionID = $(this).val();
    if(regionID){
        $.ajax({
           type:"GET",
           url:"{{route('indexpro')}}?region_id="+regionID,
           success:function(res){
            if(res){
                $("#division").empty();
                $("#division").append('<option value="">Select</option>');
                $.each(res,function(key,value){
                    $("#division").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#division").empty();
            }
           }
        });
    }else{
        $("#division").empty();
        $("#subdivision").empty();
    }
   });
    
</script>
      @endsection
