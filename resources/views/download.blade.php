

@extends('layouts.app')
  
@section('content')

@section('main-content')
   <!-- Content Wrapper. Contains page content -->
   
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
         <div class="col-md-12 col-sm-12">
            <div class="card card-info card-outline card-tabs">
            @include('inc.messages')
              <div class="card-body">
                  <div class="row">
          <!-- left column -->
          <div class="col-md-4">
            <!-- general form elements -->
            <div class="card card-info">
             <div class="card-header">
                <h3 class="card-title">Upload Content</h3>
                 <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                  </div>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" action="">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Content Title</label>
                    <input type="text" class="form-control" name="route_title" id="exampleInputEmail1" placeholder="Enter Content Title">
                  </div>

                  <div class="form-group" >
                    <label for="exampleInputEmail1">Content Type</label>
                        <select class="form-control select2" style="width: 100%;" name="class" id="region">
                            <option value="" selected>Select</option>
                            <option value="class1">Assignments</option>
                            <option value="class2">Study Material </option>
                            <option value="class3">Syllabus </option>
                            <option value="class3">Other Download </option>
                        </select>
                  </div>

                  <div class="form-group" >
                    <label for="exampleInputEmail1">Available For</label>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                            All Super Admin
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                            <label class="form-check-label" for="flexCheckChecked">
                            All Student
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
                            <label class="form-check-label" for="flexCheckChecked">
                            Available For All Classes
                            </label>
                        </div>

                  <div class="form-group" >
                    <label for="exampleInputEmail1">Class</label>
                        <select class="form-control select2" style="width: 100%;" name="class" id="region">
                            <option value="" selected>Select</option>
                            <option value="class1"></option>
                        </select>
                  </div>

                  <div class="form-group" >
                    <label for="exampleInputEmail1">Section</label>
                        <select class="form-control select2" style="width: 100%;" name="section" id="region">
                            <option value="" selected>Select</option>
                            <option value="class1"></option>
                        </select>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Upload Date</label>
                    <input type="date" class="form-control" name="route_title" id="exampleInputEmail1" placeholder="Enter Supplier">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Description</label>
                    <textarea name="description" id="" cols="10" rows="5" class="form-control" ></textarea>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Content File</label>
                    <input type="file" class="form-control" name="route_title" id="exampleInputEmail1" placeholder="">
                  </div>
                  
                  
                </div>
                <!-- /.card-body -->
                <input type="hidden" value="1" name="board_id">
               <input type="hidden" value="Baptist Board" name="board_name">
                <div class="card-footer">
                  <span class="float-right"> <button type="submit" class="btn btn-info"><i
                            class="fas fa-plus"></i>
                        Save</button></span>
                </div>
              </form>
            <!-- /.card -->
          

          </div>
</div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-8">
          <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Content List</h3>
                 <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                  </div>
              </div>
              <!-- /.card-header -->
              <!--<table class="table table-bordered table-hover dataTable table">-->
              <div class="card-body">
                <table class="display table table-striped table-bordered" id="demotable" style="width:100%"> dataTable table">
                    <thead class="thead-light">
                        <tr>
                            <th>Content Title</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Available For</th>
                            <th>Class</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                       <tr>
                        <td> </td>
                        <td> </td>
                        <td> </td>
                         
                        </tr>
                        
                    </tbody>
                    <tfoot class="tfoot-light">
                        <tr>
                            <th>Content Title</th>
                            <th>Type</th>
                            <th>Date</th>
                            <th>Available For</th>
                            <th>Class</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
              </div>
          </div>
          

           
                   </div>
               
              <!-- /.card -->
            </div>
          </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
      <!-- Main content -->
      
  <!-- /.card-body -->
          </div>
        </div>
      </div>
    
      </div>
      <!-- /.card -->
      </div></div>

        
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
@endsection

    @section('special-scripts')

  <script type="text/javascript">
    $('#region').change(function(){
    var regionID = $(this).val();
    if(regionID){
        $.ajax({
           type:"GET",
           url:"{{route('produit.drop')}}?region_id="+regionID,
           success:function(res){
            if(res){
                $("#division").empty();
                $("#division").append('<option value="">Select</option>');
                $.each(res,function(key,value){
                    $("#division").append('<option value="'+key+'">'+value+'</option>');
                });

            }else{
               $("#division").empty();
            }
           }
        });
    }else{
        $("#division").empty();
        $("#subdivision").empty();
    }
   });
    
</script>
<script>
    $(document).ready(function(){
        $('#demotable').DataTable({
            dom:'Bfrtip',
            buttons:[
                'copy','csv','excel'
            ]
        });
    });
</script>
      @endsection